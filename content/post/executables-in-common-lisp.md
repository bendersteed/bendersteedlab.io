+++
title = "Executables in Common Lisp"
date = 2018-12-24T22:58:00+02:00
tags = ["cl"]
categories = ["tech"]
draft = false
+++

So after completing (?) the Pleroma bot, I talked about in a previous
post I wanted to create a simple executable that would be
self-contained and run on any machine. If you have a Common Lisp
implementation on a computer it's trivial to run any program that is
bundled in a package, just load the package and then run the
appropriate entry function from the repl.

What happens if you don't have a Lisp installed?

<!--more-->

Well most modern Lisps have the ability to dump the current Lisp
environment in way that can be "re-run". In this way we can create
executables that have the whole implementation in them and so they are
self-contained. On this builds [Buildapp](https://www.xach.com/lisp/buildapp/), so it configures and saves a
Lisp image in an executable file.

Buildapp works with SBCL or CCL. An easy way to get it is via
quicklisp. Fire an SBCL repl and run:

```common-lisp
(ql:quickload 'buildapp)
(buildapp:build-buildapp)
```

Then you get a binary file, so put it somewhere in your PATH.

Next step is to build our executable image/file.

```bash
buildapp --output pleroma-bot \
--asdf-tree ~/quicklisp/ \
--load-system pleroma-bot \
--eval "(defun main (arg) (declare (ignore arg)) (pleroma-bot:run-bot))" \
--entry main
```

So in order here we:

-   set the output file (where our executable will be created)
-   set the folder from where to load asdf systems
-   select which system to load
-   create a wrapping function to drop arguments cause buildapp always
    passes command-line arguments to our program
-   set the "main" function of our executable

Quite easy, right? Happy Christmas everyone!
