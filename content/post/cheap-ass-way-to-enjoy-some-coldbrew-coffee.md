+++
title = "Cheap-ass way to enjoy some coldbrew coffee"
author = ["bendersteed"]
date = 2018-12-02T00:50:00+02:00
tags = ["coffee"]
categories = ["DIY"]
draft = false
+++

Ever since I read a Cory Doctorow's novel (can't remember the title it
was the one that starts with some kid going to Burning Man) I wanted
to try coldbrew coffee. Even from my first tries I was really fond of
the taste and the caffeine hit this way of preparing coffee gives.

However the preparation was something that bothered me. In summary
coldbrew coffee is very simple. Just leave some ground coffee beans in
a jar with water for some time and then filter it. Depending on the
ratio of the water-coffee mixture you get either a strong brew that
needs to be diluted for consumption or one ready. The main problem
here is filtering. It is quite boring and messy. Here I present you a
cheap-ass way of dealing with this.

<!--more-->


## How-to {#how-to}

![](/ox-hugo/IMG_20181201_234421_HHT.cleaned.jpg)
So here are the stuff that we need, namely:

-   A knife and a spoon
-   A scissors is not important, I just used it to open the coffee package
-   Coffee and some coffee filters
-   An empty water bottle
-   Some food grade string or other material to tie the filters (I used
    some plastic food grade stuff)

The whole procedure is quite easy:
![](/ox-hugo/IMG_20181201_234716_HHT.cleaned.jpg)

1.  First cut the bottle as cleanly as possible in the half.
2.  The assorted pieces will look like this:
    ![](/ox-hugo/IMG_20181201_234738_HHT.cleaned.jpg)
3.  Then put some coffee in a filter and tie them like a tea filter:
    ![](/ox-hugo/IMG_20181201_235243_HHT.cleaned.jpg)
4.  Then put some filters in the top half of the bottle and put the
    tied coffee filters in.
     ![](/ox-hugo/IMG_20181201_235500_HHT.cleaned.jpg)
5.  Pour in some water
    ![](/ox-hugo/IMG_20181201_235914_HHT.cleaned.jpg)
6.  Wait from 12-24 hours.
7.  And here comes the magic! When we think are brew is ready we can
    just open the bottles cap and the brew gets auto-filtered and
    collected in the bottom half. Yay!


## Tips {#tips}

Some tips here on how to get the best of your coldbrew coffee.

-   Find your favorite ratio. Play around with the water and coffee
    quantities. I usually go for 1:4 coffee to water ratio and then
    dilute 1:1 to consume.
-   Prefer coarsely ground beans, they seem to work better.
-   Don't store the brew in the plastic, use some glass jar.
-   Don't store for two long. I've found that after 4-5 days the taste
    isn't as good.
-   Play around with different types of coffee beans.

So that's all for now and remember to TURN ON, BOOT UP, JACK IN.
