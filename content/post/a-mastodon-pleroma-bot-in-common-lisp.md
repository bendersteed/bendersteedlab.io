+++
title = "A Mastodon/Pleroma bot in Common Lisp"
date = 2018-12-23T00:28:00+02:00
tags = ["cl"]
categories = ["tech"]
draft = false
+++

I've been trying for long to get more involved with Common Lisp as
I've felt for quite some time that it's a language with a very nice
balance of expressiveness and practicality. Together with the great
experience of developing interactively with slime and the huge
collection of libraries that exist I think that delving in Common Lisp
will be pretty exciting.

After having read Practical Common Lisp (although not yet fully
digested the knowledge) and having solved some [Advent of Code](https://gitlab.com/bendersteed/advent-of-code)
challenges I thought about writing a bot for announcing my blog-posts
in fediverse and specifically in [https://pleroma.soykaf.com/](https://pleroma.soykaf.com/). After
some research I found this [blog-spot](https://gkbrk.com/2018/08/mastodon-bot-in-common-lisp/) that gave a lot of info on
achieving almost the same goal I had.

<!--more-->

The base for the bot is two of [Shinmera's](https://github.com/Shinmera/) libraries, and drakma:

-   [tooter](https://github.com/Shinmera/tooter), a library that implements the Mastodon REST Api (that is
    compatible with the one from pleroma)
-   [plump](https://github.com/Shinmera/plump), a great parser for HTML/XML like markups
-   [drakma](https://edicl.github.io/drakma/), a full-featured HTTP client

The above libraries constitute a great foundation for creating any
kind of bot for the fediverse. The code for my bot can be found [here](https://gitlab.com/bendersteed/pleroma-bot)
and is quite easy to adapt in a series of ways to achieve different
results.

Then with cron we can run it once every day by executing:

```bash
sbcl --eval "(ql:quickload 'pleroma-bot)" --eval "(quit)"
```

Common Lisp starts to steal my mind!
