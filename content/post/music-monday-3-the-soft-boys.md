+++
title = "Music monday 3 - The Soft Boys"
author = ["bendersteed"]
date = 2018-11-30T21:47:00+02:00
tags = ["music"]
categories = ["musicmonday"]
draft = false
+++

So it's not really monday but last week has been really hectic. Well
in fact monday was setting orange and today is prickle-prickle so we
are no so far apart from our goals!

English music is where the money's at, I mean all the great periods
of music, early psychedelia,glam rock, punk, post punk, madchester,
the early 90s house/electronica, britpop and even big beat have
produced some of my favorite music.

<!--more-->

Standing peculiarly between early psychedelia and punk there are the
Soft Boys. These guys released a hundred or so great tracks and then
got spread up in bands like Katrina and the Waves (go watch the music
video where Katrina walks in sunshine if front of the foggiest bridge
of London). Well, I guess they went to Eurovision.

Robyn Hitchcock is supposed to have continued their unique style in
his solo career but I don't have listened to any of his non-Soft Boys
songs so I refrain from including him here.

So we get lyrics like:

> If your name's Him then suddenly a whim but you seem to be nowhere at all
> If your name's Her than you're covered with fur and you're waiting for Him
> in the hall
> The stuff that you sell and the way that you smell is to say the least way
> out of place
> If I had a choice between the fist and the voice you know I'd push you
> right out of your face
> But I don't mind dressing in green if I thought that you'd understand what
> I mean

That's great!

They let it all out and they fuck with your mind.
{{< youtube RCMG623jn9s  >}}

What if you want some more of the english like hope/depression? They
got you covered:
{{< youtube vmN7MGb5UXg  >}}

They even have some creepypasta songs.
{{< youtube Q2Iysm8QaG8  >}}

Laced in dreams of UK's subcultures I salute you!
