+++
title = "Retain keybindings in non-latin layouts in emacs"
author = ["bendersteed"]
date = 2018-11-13T23:30:00+02:00
tags = ["emacs"]
categories = ["tech"]
draft = false
+++

Using emacs is always a journey of finding new tricks and ways to
improve your work flow. You keep on customizing and customizing till
you declare your emacs init file bankruptcy.

One of the things that really irritated me is emacs' keybindings in a
bilingual setting. I'm using both greek and english for quite a lot of
my writings using gnome's input system to change the layout. This made
emacs keybindings unavailable if I didn't change the layout back in
english and it was quite cumbersome.

> "C-σ is undefined"

was a quite common occurrence in my work flow.

<!--more-->

What I hadn't thought of before was that emacs has an input system. So
changing languages with "C-\\" makes it so you retain thee keybindings
in any input languages.
So simple and so sweet!
