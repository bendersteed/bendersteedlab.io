+++
title = "Music monday 4 - War on drugs"
author = ["bendersteed"]
date = 2018-12-10T23:05:00+02:00
tags = ["music"]
categories = ["musicmonday"]
draft = false
+++

There are these times when every day life seems to be so hectic. Right
now between my work in the military, studying the great book "[Clojure
for the brave and true](https://www.braveclojure.com/)", solving [Advent of Code](https://adventofcode.com/) challenges as practice
to my mediocre Common Lisp skills, hacking away in Rails, trying to be
at least minimally physically active and studying math leaves little
room for my ever growing Someday to-do list.

<!--more-->

Don't get me wrong, I'm not complaining. Quite the contrary to be
honest. This time was pre-destined to come and it wasn't like this
some years ago. Some years ago I used to slack around and think about
all these great things. I thought about so many things for so long,
days and days spent doing nothing more than sharpening, or as others
may say burning, my wits. But I lost time, and now I'll get it back,
armed with dreams of two centuries ahead.

One of those sessions in creative dissonance was spend with some songs
in repeat, because none of those there was to change the playlist. And
the most distinct of the tracks was this:
{{< youtube rMToQg0vSds >}}

It has even become a recurring joke among good friends!
War on drugs are a perfectly fine band. Their music is dreamy and soft
but on the same time energetic. Like mellow mornings in the nature,
hiking, drifting along cool breezes. Or finding beauty in the concrete
abomination that is the city, in a small apartment where I used to let
my youth go to waste.
