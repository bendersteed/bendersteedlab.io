+++
title = "Music monday 1 – Galaxie 500"
author = ["bendersteed"]
date = 2018-11-12T19:32:00+02:00
tags = ["music"]
categories = ["musicmonday"]
draft = false
+++

I intend to do a music of the week series, every Monday I will blog
about one of my current and future favorite bands/artists. I would
also like to maybe create a mix-tape every now and then, a thing I
really miss since my early teens. Although then I used a real cassette
and not a list of digital files..
**Maybe I'll look around for a tape recorder..**

<!--more-->

Anyway enough with sweet nostalgia.
For the first week's band I choose Galaxie 500. I got to this band
under pretty strange circumstances. I listened to them first time
searching for warmth. That was how we (and we know who we were) went
on to walk aimlessly till we got to a friend's house that had some
air-condition and I was pretty pissed and all. But we got there and
then there is this video on, dreamy clouds and all, but this is the
band for another post.

So we got there and I was almost not talking to anyone. But as the
night went on and the algorithms of youtube went on and on, I was
feeling warmer in the body and inside and I was clearing and all, like
the light was getting smoother and pale and the sun was rising and I
was like "what would out mothers think of us?" but in a really sweet
way like the total misunderstanding you can have with those you love.

Then maybe because we understood that the music brought us to paths we
couldn't expect we noticed the band playing in the background. It was
this song:

{{< youtube vb5UjtmyOdQ >}}

All this beautiful memories and sad sad sad at the same time! That's
what these guys are. So I said enough with sweet nostalgia but I was
lying.

> You know I am, the King of Spain
>
> And I know that you can never tear me apart again
>
> The empty hall I roam around
>
> And my friends don't understand I'll never lose again

{{< youtube 5ftXavkXXRM >}}

> I went alone down to the drugstore
>
> I went in back and took a Coke
>
> I stood in line and ate my Twinkies
>
> I stood in line, I had to wait

{{< youtube k1ijNteBQwM >}}

The last video has some awkwardly fitting footage from
Rajneeshees. Synchronicity is a thing!
