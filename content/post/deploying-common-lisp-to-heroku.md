+++
title = "Deploying Common Lisp to heroku"
author = ["Dimakakos Dimos"]
date = 2019-08-12T23:21:00+03:00
tags = ["cl"]
categories = ["tech"]
draft = false
+++

Ok this is going to be a short post, but maybe someone finds it
useful so here it goes.

For testing purpose I really like the [heroku platform](https://heroku.com/), you can
deploy your application, test it, share it with others to test
etc. Heroku has some "official" languages it supports but through
the use of buildpacks you can deploy any application.

<!--more-->

Common Lisp is not among the officially supported languages but
fine hackers have taken the time to create buildpacks for it. My
experimentation with several of them brought me upon Duncan Bayne's
fine work on [his version of a buildpack](https://gitlab.com/duncan-bayne/heroku-buildpack-common-lisp/).

It works fine but with two caveats. The first one was that,
although you could select a Lisp implementation, the compile.lisp
file that was responsible for building the application was not
portable so you could only use the [CCL](https://ccl.clozure.com/) implementation.

I say this was a problem because I forked the project and added
support for [SBCL](http://www.sbcl.org/), which is my prefered Lisp. I've opened a merge
request on Gitlab but till then you can use my fork from [here](https://gitlab.com/bendersteed/heroku-buildpack-common-lisp).

The second caveat, that I haven't put the time to solve yet, is
that in order for the buildpack to work, you have to define the
initialize-application symbol in CL-USER package. This is not
really good style, I would pretty much prefer to be able to have
the function in my package. For now I workaround this caveat by
adding a heroku.lisp file to my system, that I use for the sole
purpose of defining that particular function so it can serve as an
entry point to the application. For an example look at the
structure of [this project](https://gitlab.com/bendersteed/bread-and-roses).

Woah, this took longer than expected!

Turn on, tune in, drop out..
