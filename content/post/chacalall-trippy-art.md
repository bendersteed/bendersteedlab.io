+++
title = "Chacalall's trippy art"
author = ["bendersteed"]
date = 2018-11-10T18:27:00+02:00
tags = ["art", "share"]
categories = ["social"]
draft = false
+++

Who is this guy? Her/His art is great, most trippy and comforting at the
same time. I think times like this that, this huge soup that the wired
is, gives a medium to people like her/him.
Check some of his art here and then find some more at
<http://chacalall.tumblr.com/>

<!--more-->

![](/ox-hugo/chal1.gif)
![](/ox-hugo/chal2.gif)
![](/ox-hugo/chal3.jpg)
