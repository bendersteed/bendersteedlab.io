+++
title = "Parable of the polygons"
author = ["bendersteed"]
date = 2018-11-10T15:56:00+02:00
tags = ["share", "bias"]
categories = ["social"]
draft = false
+++

Recently found this [page](https://ncase.me/polygons/).

It's a gem!
Vi Hart and Nicky Case are always in for some cool creative stuff!

The message is passed, check it even if you don't fully agree with the
underlying message. It's a nice visualization of how our society gets
segregated by bias.

<!--more-->
