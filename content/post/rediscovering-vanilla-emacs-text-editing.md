+++
title = "Rediscovering vanilla Emacs' text editing"
author = ["Dimakakos Dimos"]
date = 2019-04-22T00:01:00+03:00
tags = ["emacs"]
categories = ["tech"]
draft = false
+++

Being a full-time Emacs user for some years now I've gotten
accustomed to many packages offered by the community that greatly
enhance Emacs' text editing capabilities. The thing is that Emacs'
only lacks a good editor, or is that so?
Here are some packages I've stopped using in favor of built-in
functionality that I've discovered recently.

<!--more-->


## [expand-region.el](https://github.com/magnars/expand-region.el) - Default marking keybindings {#expand-region-dot-el-default-marking-keybindings}

Expand region is a wonderful package by Magnar Sveen of dash.el,
s.el and multiple-cursors.el fame. It is indeed very useful
because it creates regions by expanding the semantics of a
region. So in practice it would mark a word, then a sentence, then
a paragraph so on.

The thing is I was somewhat bothered by the fact that I needed to
continually expand the region till it got what I wanted. So I
decided to invest time to learn the default Emacs's' marking system
which seems to be a better fit for me:

1.  M-@ marks a word
2.  M-h marks a paragraph
3.  C-M-@ marks a balanced expression or s-expression
4.  C-M-h marks a defun (top-level-definition)
5.  C-x h marks the whole buffer

Since these semantics are enough for my needs, and the
keybindings are somewhat consistent with the M- works on word
level, C-M- on sexps and defuns paradigm of moving keybindings I
am quite happy with them.


## [yasnippet](https://github.com/joaotavora/yasnippet) - Abbrev {#yasnippet-abbrev}

Yasnippet is a well known package used to create templates for
mostly anything that has boilerplate. The native Emacs's' solution
to the same problem is Abbrev. Though not as powerful as
yasnippet, with the major advantage of yasnippet being that you
can control point position after an expansion something that
Abbrev can't. On the other hand Abbrev suggests a workflow that
really suits my own. If you recognize you write something a lot
you can instantly save it as an abbrev by C-x a g for a global or
C-x a l for a local (defined by major-mode of the buffer).
All in all since my use of snippets is not that extensive, Abbrev
mode seems to cover my needs almost fully.


## Any folding mode -- Hideshow {#any-folding-mode-hideshow}

So I haven't really tried any of the folding modes for Emacs',
mainly because this article was spawned by me wanting to try some
of them and then discovering hs-mode that is bundled with
Emacs. So hideshow can fold blocks in most programming languages
and can be expanded to work on more if they have some sane
structure in the form of begin/end blocks. It works really fine,
can be enable with M-x hs-minor-mode and then has some of the
worst keybindings that I have ever encountered. They are bad even
by Emacs' standards:

1.  C-c @ C-c Toggle block.
2.  C-c @ C-a Show all.
3.  C-c @ C-t Hide all.

If you happen to try hs-mode, do yourself a favor and change
them! Also by S-mouse-2 (often meaning middle click) you can
toggle blocks.


## Interesting discoveries {#interesting-discoveries}

Some features that seem useful and I discovered recently:

1.  C-x C-o deletes all but one consecutive blank lines, neat!
2.  M-s M-w searches for the marked region in the Web.
3.  M-$ checks the word's under point spelling, or checks a region
    if there is one.
4.  C-M-t transposes two balanced expressions.
5.  You can change case of a region with C-x C-l or C-x C-u.
6.  M-% (query-replace) also works in a region there is one, else
    works on the whole buffer.
7.  You can always be an edgy 14-year old on 00's MySpace by
    running M-x studlify-buffer!
