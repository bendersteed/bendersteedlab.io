+++
title = "How this blog works"
author = ["bendersteed"]
date = 2018-11-08T23:01:00+02:00
tags = ["hugo", "emacs"]
categories = ["tech"]
draft = false
toc = true
+++

Hello World! After the trivialities let me share some info of how this
blog is run. You can study the source in [gitlab](https://gitlab.com/bendersteed/bendersteed.gitlab.io) that is also used for
hosting the blog.

The main infrastructure here is the [hugo](https://gohugo.io/) static site generator,
[org-mode](https://orgmode.org/) for editing the posts, [ox-hugo](https://ox-hugo.scripter.co/) for exporting the org file
in hugo-compatible markdown files and the [after-dark](https://after-dark.habd.as/) theme for basic
design on which I intend to build a more personal style.

<!--more-->


## Installing Hugo on Fedora {#installing-hugo-on-fedora}

So Fedora 29 doesn't provided a recent hugo version so I installed from a [COPR](https://copr.fedorainfracloud.org/coprs/daftaupe/hugo/).
It is quite easy to do so:

```bash
sudo dnf copr enable daftaupe/hugo
sudo dnf install hugo
```

First impression: Whoa, hugo is huge, 6.3Mb!

I also want code highlighting so to install [pygments](http://pygments.org/):

```bash
sudo dnf install python2-pygments
```


## Installing [ox-hugo](https://ox-hugo.scripter.co/) {#installing-ox-hugo}

So now let's install ox-hugo in emacs, from MELPA, using ox-hugo:

```emacs-lisp
(use-package ox-hugo
  :ensure t
  :after ox
  :config
  (use-package ox-hugo-auto-export))
```

The ox-hugo-auto-export package includes some nice features to
auto-export your posts when you save your org file!


## Basic blog config {#basic-blog-config}

Now that we have a working version of hugo and ox-hugo all that we are
missing is a theme. After-dark is really nice and it has a simple
script that initializes a hugo site and install the theme in the
folder. It can be found [here](https://after-dark.habd.as/feature/quick-install/), but make sure to check the script you
are running on your machine.

Now that we have most things ready to launch, we can fiddle with the
config.toml file created from the after-dark script to suit our needs.

```toml
baseurl = "https://bendersteed.gitlab.io" # Controls base URL sitewide
languageCode = "en-US" # Controls site language
title = "Barracuda Blues" # Homepage title and page title suffix
paginate = 11 # Number of posts to show before paginating

# Controls default theme and theme components
theme = [
  "fractal-forest",
  "after-dark"
]

enableRobotsTXT = true # Suggested, enable robots.txt file
disqusShortname = "" # Deprecated, add Disqus shortname for comments

pygmentsCodefences = true # Suggested, highlight fenced code blocks
pygmentsUseClasses = true # Required for custom syntax highlighting

sectionPagesMenu = "main" # Enable menu system for lazy bloggers
footnoteReturnLinkContents = "↩" # Provides a nicer footnote return link

[params]
  description = "A blog about living in the wired and out of it!" # Suggested, controls default description meta
  author = "bendersteed" # Optional, controls author name display on posts
  hide_author = true # Optional, set true to hide author name on posts
  show_menu = true # Optional, set true to enable section menu
  powered_by = true # Optional, set false to disable footer credits
  images = [
    "https://source.unsplash.com/collection/983219/2000x1322"
  ] # Suggested, controls default Open Graph images

[params.modules.fractal_forest]
  enabled = true # Optional, set false to disable module
  decoders = ["bpgdec8a"] # Optional, 8-bit javascript decoder with animation

[[menu.main]]
  name = "About"
  weight = 3
  identifier = "about"
  url = "/about"
```


## Blogging Flow {#blogging-flow}

So in reality all the previous steps are pretty trivial. I decide to
document them just for anyone wanting to get a fast working setup. The
interesting thing is that this configuration offers an amazing
blogging procedure.

The ox-hugo package leverages all the org-mode capabilities so you can
create a post using stuff like TODO states, tag inheritance and
org-capture.

Looking into ox-hugo documentation gives some great insight on using
org to author your posts. I decided to organize my blog by having a
single blog.org file. This allows me to have some TODO headings that
will be exported as drafts but will then be published when I mark them
DONE, even getting as publish date the time the state changed. I find
this so simple yet so clever!

Another great example of how clever the ox-hugo package is, is the
fact that by setting the org-hugo-auto-export-on-save variable to
true, and having loaded the package I mentioned before, the markdown
files get auto-exported every-time I save my blog.org file.

You can add properties like toc: t to get a Table of Contents for your
posts and so many other good stuff.

For now I'm pretty happy with this configuration, and think it's quite
a step up from my previous jekyll work-flow. Going on I would like to
implement some more niceties but let's leave these for another post.

Keep flying!
