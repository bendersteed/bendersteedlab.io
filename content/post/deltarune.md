+++
title = "Deltarune"
author = ["bendersteed"]
date = 2018-11-26T00:15:00+02:00
tags = ["art", "share"]
categories = ["social"]
draft = false
+++

So in 2015 this guy called Toby Fox released a game called
Undertale. It was a hit because of it's cheesy humor and unorthodox
gameplay and all the other stuff that people loved in this game. I
played Undertale extensively and it was a really cool experience, so
I'm really happy that there is a second installment in the series. I'm
talking about [Deltarune](https://www.deltarune.com/)!

So I started playing the demo version released like 20 days ago and it
is like Undertale but better! Read more only if you don't care about
spoilers, else go play the game and return here later!

<!--more-->

In Deltarune we are Kris, not Frisk but also not-really-in-the-mood
for talking living with someone like Toriel (you killed
her before remember?) and doing normal stuff, with monsters living on the
surface of Earth! After some incident that includes eating chalk and
getting stuck in a dark closet with bully monster-girl named Susie we
get in a world of darkness with the aforementioned girl.

There we meet Ralsei the prince of dark, that is a cuddly boy! And we
setup for an adventure with cheeky humor and love spreading!
![](/ox-hugo/delta2.PNG)
Who wouldn't like some Ralsei?

Susie is really cool company for this adventure. Susie is edgy all
the time, eating poor people's cake and smashing everyone that gets in
her way with her ax. When we solve a puzzle that opens a door so we
won't get impaled on a fence she says:

> Damn, I didn't get to impale myself

Oh, Susie...
![](/ox-hugo/delta1.PNG)

Or when we choose to let her name our squad and poor Ralsei couldn't
even say it..
![](/ox-hugo/delta3.PNG)

So we learn by Ralsei that the king of darkness has become nuts and
wants to spread darkness everywhere. So we are the heroes to stop him
but first we have to get through his trying-to-be-scary son Lancer.
Not that difficult in fact but he puts on some work after he gets
inspired by Susie. Especially when they join forces as Susie finds
that being on the side of the bad guys is:
![](/ox-hugo/delta4.PNG)

All in all the game is really funny and even seems to offer some
improvements to Undertale. For once the battle system seems a little
less repetitive, more akin to a Final Fantasy game. The soundtrack is
amazing and the plot seems to thicken a lot after the end of this
first chapter. I'm really eager for the full release, and I really
hope that there is a linux version like Undertale as well since my
only gripe right now is that I have to play on Windows. But I hope
this will get fixed since everything can be fixed..
![](/ox-hugo/delta5.PNG)
