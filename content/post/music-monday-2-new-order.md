+++
title = "Music monday 2 - New Order"
author = ["bendersteed"]
date = 2018-11-19T21:10:00+02:00
tags = ["music"]
categories = ["musicmonday"]
draft = false
+++

Oh, New Order.. Since the first time I heard "Temptation" sung by
Mark's underage girlfriend in Trainspotting they have been a constant
in my music tastes. The post-punk aesthetic, the dance elements and
the "I'm trying so hard to not make sense" lyrics are a combination
that I can't get over, so I return and return and return listening to
their music and re-discovering so many of their great tracks, every
once in a while.

<!--more-->

So it is intimate time again, isn't it? There was this time that I was
really preparing for changes. Haven't you been there? This time I was
alone and I was the enemy. There was just this confusion, actions that
were incomprehensible, bad habits not programmed to change. And then
boom!

It was early in the morning waking in a profound sense of dread and
awe, like the mornings you want to cry and laugh and then cry some
more because why do they still keep the machine turning and what cog
am I supposed to be and don't you know that people are born free?

So I'm in this state and someone around me is getting all
uncomfortable, but hey "don't you love New Order?". But no the sense
was not shared so it was a lonely path, a road to be taken by one. So
in the all-devouring morning the hope that sparkled and fell like
lightning and left me laughing was oh-so sincere so I called my family
and friends crying of happiness and resentment cause they were there
in my lonely path.

And this was the soundtrack of that moment:
{{< youtube 0NZfjbht79M  >}}

> And I'm not the kind that likes to tell you
>
> Just what you want me to

{{< youtube 8ahU-x-4Gxw >}}

> Up, down, turn around
>
> Please don't let me hit the ground
>
> Tonight I think I'll walk alone
>
> I'll find my soul as I go home

{{< youtube liix4FQEaUw  >}}

Till the next time, TURN ON-TUNE IN-DROP OUT..
