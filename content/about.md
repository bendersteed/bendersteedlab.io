+++
title = "About"
date = 2018-11-07T22:40:00+02:00
draft = false
[menu.main]
  weight = 1001
  identifier = "about"
+++

Hello there, I'm going around by the handle bendersteed in the wired,
since everywhere. I got a thing for technology while at the same time
being charmed by Luddism.

This blog is a collection of my thoughts and things I learn. My
interests include programming, the web and networking, HAM radio,
martial arts and nature. I'm also into DIY in a lot of my every day
needs.

This blog is generated through a single org-mode file, using hugo,
ox-hugo and emacs! You can find more about my blogging setup
[here](/post/how-this-blog-works/).

Thanks for passing by and I hope to see you again!
