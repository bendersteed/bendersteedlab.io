# After Dark

> After Dark is a hypermedia authoring toolkit built on Hugo designed to create the world's fastest, most scalable websites.

[![Latest NPM version](https://img.shields.io/npm/v/after-dark.svg?style=flat-square)](https://www.npmjs.com/package/after-dark)
[![Monthly downloads](https://img.shields.io/npm/dm/after-dark.svg?style=flat-square)](https://www.npmjs.com/package/after-dark)
[![Minimum Hugo version](https://img.shields.io/badge/hugo->%3D%200.44-FF4088.svg?style=flat-square)](https://gohugo.io)
[![WTFPL licensed](https://img.shields.io/npm/l/after-dark.svg?style=flat-square&longCache=true)](https://git.habd.as/comfusion/after-dark/src/branch/master/COPYING)
[![Telegram chat](https://img.shields.io/badge/chat-telegram-32AFED.svg?style=flat-square&longCache=true)](https://t.me/comfusion)
[![Keybase profile](https://img.shields.io/badge/pm-keybase-4c8eff.svg?style=flat-square&longCache=true)](https://keybase.io/jhabdas)

## Features

- [Web Mining](https://after-dark.habd.as/#feature-mining)
- [Advanced Graphics](https://after-dark.habd.as/#feature-graphics)
- [Fuzzy Search](https://after-dark.habd.as/#feature-search)
- [Ludicrous Speed](https://after-dark.habd.as/#feature-speed)
- [Easily Customized](https://after-dark.habd.as/#feature-customize)
- [Securely Designed](https://after-dark.habd.as/#feature-security)

Visit the [Online Help](https://after-dark.habd.as) docs to learn

## Screenshots

<table role="presentation">
  <tr>
    <td>
      <a target="_blank" href="https://after-dark.habd.as/images/screenshots/example-landing-page-fs8.png">
        <img alt="Example Landing Page screenshot" src="https://after-dark.habd.as/images/screenshots/example-landing-page-fs8.png">
      </a>
    </td>
    <td>
      <a target="_blank" href="https://after-dark.habd.as/images/screenshots/feature-online-help-fs8.png">
        <img alt="Online Help screenshot" src="https://after-dark.habd.as/images/screenshots/feature-online-help-fs8.png">
      </a>
    </td>
    <td>
      <a target="_blank" href="https://after-dark.habd.as/images/screenshots/feature-error-page-fs8.png">
        <img alt="Error Page screenshot" src="https://after-dark.habd.as/images/screenshots/feature-error-page-fs8.png">
      </a>
    </td>
  </tr>
  <tr>
    <th scope="col">Landing Page Example</th>
    <th scope="col">Online Help</th>
    <th scope="col">Error Page</th>
  </tr>
</table>

<table role="presentation">
  <tr>
    <td>
      <a target="_blank" href="https://after-dark.habd.as/images/screenshots/module-toxic-swamp-fs8.png">
        <img alt="Webmining screenshot" src="https://after-dark.habd.as/images/screenshots/module-toxic-swamp-fs8.png">
      </a>
    </td>
    <td>
      <a target="_blank" href="https://after-dark.habd.as/images/screenshots/shortcode-button-fs8.png">
        <img alt="Form Controls screenshot" src="https://after-dark.habd.as/images/screenshots/shortcode-button-fs8.png">
      </a>
    </td>
    <td>
      <a target="_blank" href="https://after-dark.habd.as/images/screenshots/extra-high-tea-fs8.png">
        <img alt="Extras screenshot" src="https://after-dark.habd.as/images/screenshots/extra-high-tea-fs8.png">
      </a>
    </td>
  </tr>
  <tr>
    <th scope="col">Webmining</th>
    <th scope="col">Form Controls</th>
    <th scope="col">Extras</th>
  </tr>
</table>

## Getting Started

Please [Install Hugo](https://gohugo.io/getting-started/installing) `0.44` or greater before getting started.

### Installation

For scripted installation please use [Quick Install](https://after-dark.habd.as/feature/quick-install/). Otherwise download a copy and:

```sh
hugo serve --theme after-dark
```

### Upgrading

Run the [Upgrade Script](https://after-dark.habd.as/feature/upgrade-script/) anytime to check for updates and automatically upgrade to the latest version.

### Verifying

If installed or upgraded via script you may use the [Release Validator](https://after-dark.habd.as/validate/) to verify you're running a PGP-signed and SHA-verified release.

### Help

[Online Help](https://after-dark.habd.as/feature/online-help/) may be served locally with `hugo --source themes/after-dark/docs` upon installation and hosted online at after-dark.habd.as. WYSIWYG.

## License

Copyright (C) 2016–2018 Josh Habdas <jhabdas@protonmail.com>

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.

## Acknowledgements

Thanks to Steve Francia for creating the Hugo logo, エゴイスト for hackcss, Dan Klammer for the bytesized SVG icons, Alexander Farkas for killing it with lazySizes, Simon Fremaux for such a groovy 404 background animation and Vincent Prouillet for the [Gutenberg port](https://www.getgutenberg.io/themes/after-dark/).
